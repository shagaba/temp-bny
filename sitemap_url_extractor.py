import os
import logging
import requests

from bs4 import BeautifulSoup
from pprint import pprint

_REQUEST_TIMEOUT = 15

# logging
logging.basicConfig(level=logging.DEBUG)
SRH__CRAWLERS__LOG_LEVEL = os.getenv("SRH__CRAWLERS__LOG_LEVEL", default="INFO")
logger = logging.getLogger("crawlers.catalyst.sitemap")
logger.setLevel(SRH__CRAWLERS__LOG_LEVEL)


def get_sitemap_xml(sitemap_url):
    response = requests.get(sitemap_url, verify=False)
    xml = BeautifulSoup(response.content, 'lxml-xml', from_encoding=response.encoding)
    return xml


def get_sitemapindex_url_dictionary(xml):
    sitemaps = xml.find_all('sitemap')
    output = []
    for sitemap in sitemaps:

        url_dict = {'loc': None, 'lastmod': None}

        if sitemap.find('loc'):
            url_dict['loc'] = sitemap.find('loc').text

        if sitemap.find('lastmod'):
            url_dict['lastmod'] = sitemap.find('lastmod').text

        output.append(url_dict)
    return output


def sitemap_url_dictionary(xml, name=None):
    urls = xml.find_all('url')
    output = []
    for url in urls:

        url_dict = {'loc': None, 'lastmod': None, 'changefreq': None, 'priority': None, 'sitemap': name}

        if url.find('loc'):
            url_dict['loc'] = url.find('loc').text

        if url.find('lastmod'):
            url_dict['lastmod'] = url.find('lastmod').text

        if url.find('changefreq'):
            url_dict['changefreq'] = url.find('changefreq').text

        if url.find('priority'):
            url_dict['priority'] = url.find('priority').text

        output.append(url_dict)
    return output


def get_all_urls(sitemap_url):
    sitemap_xml = get_sitemap_xml(sitemap_url)
    sitemap_url_dict = sitemap_url_dictionary(sitemap_xml, name=sitemap_url)
    sitemapindex_url_dict = get_sitemapindex_url_dictionary(sitemap_xml)

    for child_sitemapindex_url in sitemapindex_url_dict:
        sitemap_url_dict.append(get_all_urls(child_sitemapindex_url['loc']))

    return sitemap_url_dict


if __name__ == '__main__':
    pprint(get_all_urls('https://wordpress.com/sitemap.xml'))

