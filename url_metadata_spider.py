
from pprint import pprint

import scrapy


def filter_list(str_list):
    # strip the list
    strings = [i.strip() for i in str_list]
    # remove empty strings from the list using list comprehension (the most Pythonic way)
    return [s for s in strings if s]


class UrlMetadataSpider(scrapy.Spider):
    name = 'url-metadata'

    def start_requests(self):
        with open("./resources/urls.txt") as f:
            urls = [url.strip() for url in f.readlines()]

        print(urls)

        for url in urls:
            yield scrapy.Request(url=url, method='HEAD', callback=self.parse)
            yield scrapy.Request(url=url, method='GET', callback=self.parse)

    def parse(self, response, **kwargs):
        pprint('-- START PARSE --')
        if 'HEAD' == response.request.method.upper():
            pprint(self.parse_head(response=response, kwargs=kwargs))
        elif 'GET' == response.request.method.upper():
            pprint(self.parse_get(response=response, kwargs=kwargs))

        # head_dictionary = self.parse_head(response=response, kwargs=kwargs)
        # get_dictionary = self.parse_get(response=response, kwargs=kwargs)

        pprint('-- END PARSE --')

    def parse_head(self, response, kwargs):
        # pprint('-- START PARSE HEAD --')
        address = response.url
        address_len = len(address)
        request_method = response.request.method.upper()
        content_type = response.headers['Content-Type']
        status_code = response.status
        headers = response.headers
        meta = response.meta
        meta_download_latency = response.meta['download_latency']
        meta_download_timeout = response.meta['download_timeout']
        return {
            'address': address,
            'addressLength': address_len,
            'request_method': request_method,
            'contentType': content_type,
            'statusCode': status_code,
            'headers': headers,
            'meta': meta,
            'metaDownloadLatency': meta_download_latency,
            'metaDownloadTimeout': meta_download_timeout
        }
        # print('-- END PARSE HEAD --')

    def parse_get(self, response, **kwargs):
        # pprint('-- START PARSE GET --')
        address = response.url
        address_len = len(address)
        request_method = response.request.method.upper()
        content_type = response.headers['Content-Type']
        status_code = response.status
        headers = response.headers
        meta = response.meta
        title = response.css('title::text').get()
        title_len = len(title)
        description = response.xpath("//meta[@name='description']/@content").get()
        description_len = len(description) if description else 0
        keywords = response.xpath("//meta[@name='keywords']/@content").get()
        h1 = filter_list(response.xpath('//h1//text()').getall())
        h2 = filter_list(response.xpath('//h2//text()').getall())
        h3 = filter_list(response.xpath('//h3//text()').getall())
        robot = response.xpath("//meta[@name='robots']/@content").get()
        meta_download_latency = response.meta['download_latency']
        meta_download_timeout = response.meta['download_timeout']
        return {
            'address': address,
            'addressLength': address_len,
            'request_method': request_method,
            'contentType': content_type,
            'statusCode': status_code,
            'headers': headers,
            'meta': meta,
            'title': title,
            'titleLength': title_len,
            'metaDescription': description,
            'metaDescriptionLength': description_len,
            'metaKeywords': keywords,
            'h1': h1,
            'h2': h2,
            'h3': h3,
            'robot': robot,
            'metaDownloadLatency': meta_download_latency,
            'metaDownloadTimeout': meta_download_timeout
        }
        # print('-- END PARSE GET --')
